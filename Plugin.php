<?php namespace Arpix\ApiGenerator;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public $require = [
        'RainLab.Builder',
        'Vdomah.JWTAuth'
    ];
    
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }
}
