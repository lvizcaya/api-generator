<?php namespace Arpix\ApiGenerator\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateArpixApigeneratorData extends Migration
{
    public function up()
    {
        Schema::create('arpix_apigenerator_data', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('endpoint');
            $table->string('model');
            $table->string('description');
            $table->text('custom_format');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('arpix_apigenerator_data');
    }
}
