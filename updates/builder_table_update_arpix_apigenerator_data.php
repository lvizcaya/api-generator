<?php namespace Arpix\ApiGenerator\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArpixApigeneratorData extends Migration
{
    public function up()
    {
        Schema::table('arpix_apigenerator_data', function($table)
        {
            $table->boolean('secure')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('arpix_apigenerator_data', function($table)
        {
            $table->dropColumn('secure');
        });
    }
}
